package net.nishidentsu.teleark2
import android.annotation.SuppressLint
import android.app.*
import android.content.Context
import android.content.Intent
import android.media.RingtoneManager
import android.os.Build
import android.os.Handler
import android.os.PowerManager
import android.view.View
import android.widget.Toast
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationCompat.PRIORITY_MAX
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import kotlinx.android.synthetic.main.activity_main.*
import java.util.*


class PushNotificationListenerService:FirebaseMessagingService() {

    // 新しいトークンが生成された時の処理
    // 中でサーバにトークンを送信する処理などを定義
//    override fun onNewToken(p0: String?) {
//        super.onNewToken(p0)
//
//        // チャンネルidを設定
//        addChannelId()
//    }

    private var keyguard: KeyguardManager? = null
    private var keylock: KeyguardManager.KeyguardLock? = null
    private var wakelock: PowerManager.WakeLock? = null


    override fun onNewToken(p0: String) {

        val pref = getSharedPreferences("DataStore", Context.MODE_PRIVATE)
        val editor = pref.edit()
        editor.putString("device_token",p0)
        editor.apply()
        super.onNewToken(p0)
        addChannelId()
    }

    fun setAlarm(context: Context) {
        // 時間をセットする
        val calendar = Calendar.getInstance()
        // Calendarを使って現在の時間をミリ秒で取得
        calendar.timeInMillis = System.currentTimeMillis()
        // 5秒後に設定
        calendar.add(Calendar.SECOND, 5)

        //明示的なBroadCast
        val intent = Intent(
            applicationContext,
            AlarmReceiver::class.java
        )
        val pendingIntent = PendingIntent.getBroadcast(
            applicationContext, 1, intent, 0
        )

        //アラームクロックインフォを作成してアラーム時間をセット
        val clockInfo = AlarmManager.AlarmClockInfo(calendar.timeInMillis, null)
        //アラームマネージャーにアラームクロックをセット
        (context.getSystemService(Context.ALARM_SERVICE) as AlarmManager).setAlarmClock(clockInfo, pendingIntent)
    }

    @SuppressLint("InvalidWakeLockTag")
    override fun onMessageReceived(p0: RemoteMessage) {
        super.onMessageReceived(p0)
                // 今回は通知からタイトルと本文を取得
        val data = p0.data
        val title: String = p0?.notification?.title.toString()
        val text: String = data["notification_text"].toString()


        setAlarm(this)

        if(text.contains("解錠しました"))
        {
            val pref = getSharedPreferences("DataStore", Context.MODE_PRIVATE)
            val editor = pref.edit()
            if(!pref.getBoolean("isOutou",false))
            {
                sendNotification2("通知", text)
                val intent = Intent("KaijyouEvent")
                editor.putString("pushMsg",text)
                editor.apply()
                LocalBroadcastManager.getInstance(this).sendBroadcast(intent)
            }


        }

        else if(text.contains("応答しました"))
        {
            val pref = getSharedPreferences("DataStore", Context.MODE_PRIVATE)

            if(!pref.getBoolean("isOutou",false)) {
                sendNotification2("通知", text)
                val intent = Intent("OutouEvent")

                LocalBroadcastManager.getInstance(this).sendBroadcast(intent)
            }

        }
        else if(text.contains("呼び出しがありました"))
        {
            val pref = getSharedPreferences("DataStore", Context.MODE_PRIVATE)
            if(!pref.getBoolean("isOutou",false)) {
                sendNotification2("通知", text)
            }


        }
        else{
            val data = p0.data

            val intent = Intent(applicationContext,MainActivity::class.java)
            val pendingIntent = PendingIntent.getActivity(applicationContext,0,intent,0)

            val pref = getSharedPreferences("DataStore",Context.MODE_PRIVATE)
            val editor = pref.edit()
            editor.putBoolean("isChakushin",true)
            editor.putString("chakushinMsg",data["notification_text"].toString())
            editor.apply()


            if(AppProcessStatus.current(this) == AppProcessStatus.FOREGROUND)
            {
                editor.putBoolean("isForegroundChakushin",true)
                editor.apply()
            }
            else if(AppProcessStatus.current(this) == AppProcessStatus.GONE){


                // 通知表示


                wakelock = (getSystemService(Context.POWER_SERVICE) as PowerManager)
                    .newWakeLock(
                        PowerManager.FULL_WAKE_LOCK
                                or PowerManager.ACQUIRE_CAUSES_WAKEUP
                                or PowerManager.ON_AFTER_RELEASE, "disableLock"
                    )
                wakelock!!.acquire(200000)

                keyguard = getSystemService(Context.KEYGUARD_SERVICE) as KeyguardManager
                keylock = keyguard!!.newKeyguardLock("disableLock")
                keylock!!.disableKeyguard()

                sendNotification2("通知", data["notification_text"].toString())

                pendingIntent.send()

            }
            else if(AppProcessStatus.current(this) == AppProcessStatus.BACKGROUND){

                moveTaskToFront()
                moveTaskToFront()
                moveTaskToFront()
                wakelock = (getSystemService(Context.POWER_SERVICE) as PowerManager)
                    .newWakeLock(
                        PowerManager.FULL_WAKE_LOCK
                                or PowerManager.ACQUIRE_CAUSES_WAKEUP
                                or PowerManager.ON_AFTER_RELEASE, "disableLock"
                    )
                wakelock!!.acquire(200000)

                keyguard = getSystemService(Context.KEYGUARD_SERVICE) as KeyguardManager
                keylock = keyguard!!.newKeyguardLock("disableLock")
                keylock!!.disableKeyguard()

                wakelock = (getSystemService(Context.POWER_SERVICE) as PowerManager)
                    .newWakeLock(
                        PowerManager.FULL_WAKE_LOCK
                                or PowerManager.ACQUIRE_CAUSES_WAKEUP
                                or PowerManager.ON_AFTER_RELEASE, "disableLock"
                    )
                wakelock!!.acquire(200000)

                keyguard = getSystemService(Context.KEYGUARD_SERVICE) as KeyguardManager
                keylock = keyguard!!.newKeyguardLock("disableLock")
                keylock!!.disableKeyguard()


                moveTaskToFront()


            }else
            {
                wakelock = (getSystemService(Context.POWER_SERVICE) as PowerManager)
                    .newWakeLock(
                        PowerManager.FULL_WAKE_LOCK
                                or PowerManager.ACQUIRE_CAUSES_WAKEUP
                                or PowerManager.ON_AFTER_RELEASE, "disableLock"
                    )
                wakelock!!.acquire(200000)

                keyguard = getSystemService(Context.KEYGUARD_SERVICE) as KeyguardManager
                keylock = keyguard!!.newKeyguardLock("disableLock")
                keylock!!.disableKeyguard()
                sendNotification2("通知", data["notification_text"].toString())

                pendingIntent.send()
//                startActivity(intent)
            }



        }


    }

    private fun moveTaskToFront() {
        val id = getMyAppId()
        if (id > 0) {
            val activityManager =
                getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager
            activityManager.moveTaskToFront(id, ActivityManager.MOVE_TASK_NO_USER_ACTION)
        }
    }

    /**
     * @return 自分のアプリのId
     *
     * マイナス値の場合は自分自身が起動していない
     */
    private fun getMyAppId(): Int {
        val id = -1
        val activityManager =
            getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager
        val recentTasks =
            activityManager.getRunningTasks(Int.MAX_VALUE)
        for (i in recentTasks.indices) {
            if (recentTasks[i].baseActivity!!.packageName == this@PushNotificationListenerService.getPackageName()) {
                return recentTasks[i].id
            }
        }
        return id
    }
    // 通知を受信したときの処理
//    override fun onMessageReceived(message: RemoteMessage?) {
//        super.onMessageReceived(message)
//
//        // 今回は通知からタイトルと本文を取得
//        val title: String = message?.notification?.title.toString()
//        val text: String = message?.notification?.body.toString()
//
//        // 通知表示
//        sendNotification(title, text)
//    }

    // 通知表示 および 見た目の設定
    private fun sendNotification(title: String, text: String) {
        // 通知タップ時に遷移するアクティビティを指定

        val intent = Intent(this, MainActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        // 何度も遷移しないようにする（1度だけ！）
        val pendingIntent: PendingIntent = PendingIntent.getActivity(this,0, intent, PendingIntent.FLAG_ONE_SHOT)

        // 通知メッセージのスタイルを設定（改行表示に対応）
        val inboxStyle = NotificationCompat.InboxStyle()
        val messageArray: List<String> = text.split("\n")
        for (msg: String in messageArray) {
            inboxStyle.addLine(msg)
        }

        // 通知音の設定
        val defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)

        // 通知の見た目を設定
        val notificationBuilder
                = NotificationCompat.Builder(this, resources.getString(R.string.default_notification_channel_id))
            .setContentTitle(title)
            .setContentText(text)
            // ステータスバーに表示されるアイコン
            .setSmallIcon(R.drawable.ic_launcher_foreground)
            // 上で設定したpendingIntentを設定
            // 優先度を最大化
            .setPriority(PRIORITY_MAX)
            // 通知音を出すように設定
            .setSound(defaultSoundUri)

        // 通知を実施
        // UUIDを付与することで各通知をユニーク化
        val notificationManager: NotificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        val uuid = UUID.randomUUID().hashCode()
        notificationManager.notify(uuid, notificationBuilder.build())

        // Android 8.0 以上はチャンネル設定 必須
        // strings.xml に channel_id を指定してください
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            notificationBuilder.setChannelId(resources.getString(R.string.default_notification_channel_id))
        }
    }

    private fun sendNotification2(title: String, text: String) {
        // 通知タップ時に遷移するアクティビティを指定

        // 通知メッセージのスタイルを設定（改行表示に対応）
        val inboxStyle = NotificationCompat.InboxStyle()
        val messageArray: List<String> = text.split("\n")
        for (msg: String in messageArray) {
            inboxStyle.addLine(msg)
        }

        // 通知音の設定
        val defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)

        // 通知の見た目を設定
        val notificationBuilder
                = NotificationCompat.Builder(this, resources.getString(R.string.default_notification_channel_id))
            .setContentTitle(title)
            .setContentText(text)
            // ステータスバーに表示されるアイコン
            .setSmallIcon(R.drawable.ic_launcher_foreground)
            // 上で設定したpendingIntentを設定
            // 優先度を最大化
            .setPriority(PRIORITY_MAX)
            // 通知音を出すように設定
            .setSound(defaultSoundUri)

        // 通知を実施
        // UUIDを付与することで各通知をユニーク化
        val notificationManager: NotificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        val uuid = UUID.randomUUID().hashCode()
        notificationManager.notify(uuid, notificationBuilder.build())

        // Android 8.0 以上はチャンネル設定 必須
        // strings.xml に channel_id を指定してください
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            notificationBuilder.setChannelId(resources.getString(R.string.default_notification_channel_id))
        }
    }

    // チャンネルの設定
    private fun addChannelId() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val manager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            // ヘッドアップ通知を出す場合はチャンネルの重要度を最大にする必要がある
            val channel = NotificationChannel(
                resources.getString(R.string.default_notification_channel_id),
                resources.getString(R.string.channel_name),
                NotificationManager.IMPORTANCE_HIGH
            )

            // ロック画面における表示レベル
            channel.lockscreenVisibility = Notification.VISIBILITY_PUBLIC
            // チャンネル登録
            manager.createNotificationChannel(channel)
        }
    }
}