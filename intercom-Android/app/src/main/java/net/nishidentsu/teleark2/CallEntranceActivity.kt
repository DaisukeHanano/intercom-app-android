package net.nishidentsu.teleark2

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.MenuItem
import android.view.View
import android.widget.TextView
import com.github.kittinunf.fuel.httpPost
import com.github.kittinunf.fuel.json.responseJson
import com.github.kittinunf.result.Result
import org.json.JSONArray
import org.json.JSONObject

class CallEntranceActivity : AppCompatActivity() {

    val get_entrance_list_url = "https://webrtc.nishi-dentsu.com/teleark/api/GetEntranceList.php"

    var jsonAry : JSONArray? = JSONArray()
    @SuppressLint("ResourceAsColor")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_call_entrance)

        val pref = getSharedPreferences("DataStore", Context.MODE_PRIVATE)
        val id = pref.getString("id","")
        get_entrance_list_url.httpPost(listOf("user_id" to id)).responseJson { request, response, result ->
            when (result) {
                is Result.Success -> {
                    jsonAry = result.value.array()

                    Log.d("aaa", jsonAry!!.length().toString())



                    for (i in 0..(jsonAry!!.length() - 1)) {
                        print(i)
                        var tv: TextView? = TextView(this)

                        if (i == 0) {
                            tv = findViewById(R.id.EntranceTV1)
                        } else if (i == 1) {
                            tv = findViewById(R.id.EntranceTV2)
                        } else if (i == 2) {
                            tv = findViewById(R.id.EntranceTV3)
                        } else if (i == 3) {
                            tv = findViewById(R.id.EntranceTV4)
                        } else if (i == 4) {
                            tv = findViewById(R.id.EntranceTV5)
                        }
                        val json = jsonAry!![i] as JSONObject
                        val name = json["name"] as String

                        runOnUiThread {
                            tv!!.text = name
                            tv!!.setBackgroundColor(Color.LTGRAY)
                        }
                    }
                }

                is Result.Failure -> {

                }
            }
        }
    }

    override fun onResume() {
        super.onResume()
        supportActionBar!!.title = "玄関機呼出"
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if(item.itemId == android.R.id.home)
        {
            finish()
            return  true
        }
        return super.onOptionsItemSelected(item)

    }
    public fun EntranceButtonTapped(view: View)
    {
        if(view == findViewById(R.id.EntranceTV1))
        {
            if(jsonAry!!.length() > 0)
            {
                print("TV1")
                val json = jsonAry!![0] as JSONObject
                val intent = Intent()
                intent.putExtra("peer_id",json["entrance_peer_id"] as String)
                setResult(999,intent)
                finish()
            }


        }
        else if(view == findViewById(R.id.EntranceTV2))
        {
            if(jsonAry!!.length() > 1)
            {
                val json = jsonAry!![1] as JSONObject
                print("TV2")
                val intent = Intent(application,HistoryDetailActivity::class.java)
                intent.putExtra("peer_id",json["entrance_peer_id"] as String)
                setResult(999,intent)
                finish()
            }

        }
        else if(view == findViewById(R.id.EntranceTV3))
        {
            if(jsonAry!!.length() > 2)
            {
                val json = jsonAry!![2] as JSONObject
                print("TV3")
                val intent = Intent(application,HistoryDetailActivity::class.java)
                intent.putExtra("peer_id",json["entrance_peer_id"] as String)
                setResult(999,intent)
                finish()
            }

        }
        else if(view == findViewById(R.id.EntranceTV4))
        {
            if(jsonAry!!.length() > 3)
            {
                val json = jsonAry!![3] as JSONObject
                print("TV4")
                val intent = Intent(application,HistoryDetailActivity::class.java)
                intent.putExtra("peer_id",json["entrance_peer_id"] as String)
                setResult(999,intent)
                finish()
            }

        }
        else if(view == findViewById(R.id.EntranceTV5))
        {
            if(jsonAry!!.length() > 4)
            {
                val json = jsonAry!![4] as JSONObject
                print("TV5")
                val intent = Intent(application,HistoryDetailActivity::class.java)
                intent.putExtra("peer_id",json["entrance_peer_id"] as String)
                setResult(999,intent)
                finish()
            }

        }
    }
}