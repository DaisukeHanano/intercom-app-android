package net.nishidentsu.teleark2


import android.Manifest
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.Settings
import android.telephony.TelephonyManager
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.github.kittinunf.fuel.httpPost
import com.github.kittinunf.fuel.json.responseJson
import com.github.kittinunf.result.Result
import kotlinx.android.synthetic.main.activity_login.*

class LoginActivity : AppCompatActivity(){

    val login_url = "https://webrtc.nishi-dentsu.com/teleark/api/CheckUser.php"
    val setDevice_url = "https://webrtc.nishi-dentsu.com/teleark/api/SetDeviceID.php"
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        val permission = ContextCompat.checkSelfPermission(this,
            Manifest.permission.READ_PHONE_STATE)

        if (permission != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.READ_PHONE_STATE), 1)
        }

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
        {

            if(!Settings.canDrawOverlays(this))
            {
                val intent = Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION, Uri.parse("package:" + packageName))
                startActivity(intent)
            }

        }

        val pref = getSharedPreferences("DataStore",Context.MODE_PRIVATE)
//        val editor: SharedPreferences.Editor = pref.edit()
//        editor.clear()
//        editor.commit()
        if(pref.getBoolean("isLogin",false))
        {
            val intent = Intent(this,MainActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_CLEAR_TASK
            startActivity(intent)
            finish()
        }
    }
    fun setDeviceID(deviceID:String,deviceToken:String){

        setDevice_url.httpPost(listOf("id" to idEditText.text,"pass" to passEditText.text,"device_id" to deviceID,"device_token" to deviceToken)).responseJson { request, response, result ->
            when(result)
            {
                is Result.Success->{
//                    print(String(response.data))
//                    print(String(response.data))
                    val json = result.value.obj()


                    Log.d("json_res",json["user_pass"].toString())

                }

                is Result.Failure->{
                    print("通信に失敗しました。")
                }
            }
        }
    }

    public fun KiyakuButtonTapped(view: View) {
        val uri = Uri.parse("http://www.nishi-dentsu.co.jp/date/riyoukiyaku.pdf")
        val intent = Intent(Intent.ACTION_VIEW,uri)
        startActivity(intent)
    }

    public fun CheckTapped(view: View) {
        OKBtn.isEnabled = kiyakuCheck.isChecked
    }

    @RequiresApi(Build.VERSION_CODES.O)
    public fun LoginButtonTapped(view: View) {

        login_url.httpPost(listOf("id" to idEditText.text,"pass" to passEditText.text)).responseJson { request, response, result ->
            when(result)
            {
                is Result.Success->{
                    val json = result.value.obj()

                    if(json["status"] == 0)
                    {
                        //　
                        val manager = getSystemService(Context.TELEPHONY_SERVICE) as TelephonyManager
                        if (ActivityCompat.checkSelfPermission(
                                this,
                                Manifest.permission.READ_PHONE_STATE
                            ) != PackageManager.PERMISSION_GRANTED
                        ) {
                            // TODO: Consider calling
                            //    ActivityCompat#requestPermissions
                            // here to request the missing permissions, and then overriding
                            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                            //                                          int[] grantResults)
                            // to handle the case where the user grants the permission. See the documentation
                            // for ActivityCompat#requestPermissions for more details.

                        }
                        else{
                            val pref = getSharedPreferences("DataStore",Context.MODE_PRIVATE)
                            val editor = pref.edit()

                            var deviceId = ""
                            if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                                deviceId = Settings.Secure.getString(
                                    contentResolver,
                                    Settings.Secure.ANDROID_ID);
                            } else {
                                deviceId = manager.getImei()
                            }
                            editor.putString("deviceid",deviceId)
                            editor.apply()
                            val device_token = pref.getString("device_token","")
                            if(json["device_id"] == "")
                            {
                                setDeviceID(deviceId,device_token!!)
                                runOnUiThread {
                                    AlertDialog.Builder(this) // FragmentではActivityを取得して生成
                                        .setTitle("ようこそ!")
                                        .setMessage("建物名:" + json["name"].toString() + "\n部屋:" + json["room_no"].toString())
                                        .setPositiveButton("OK", { dialog, which ->
                                            // TODO:Yesが押された時の挙動


                                            val property_id = json["property_id"].toString()
                                            editor.putBoolean("isLogin",true)
                                            editor.putString("buildingName",json["name"].toString())
                                            editor.putString("room_no",json["room_no"].toString())
                                            editor.putString("id",json["user_id"].toString())
                                            editor.putString("tel",json["tel"].toString())
                                            editor.putString("user_name",json["user_name"].toString())
                                            editor.putString("property_id",json["property_id"].toString())
                                            editor.putString("isMantion2",
                                                json["isMantion"].toString()
                                            )
                                            editor.apply()
                                            val intent = Intent(application,MainActivity::class.java)
                                            startActivity(intent)
                                        })
                                        .show()
                                }
                            }
                            else
                            {
                                if ( json["device_id"] != deviceId )
                                {
                                    runOnUiThread {
                                        AlertDialog.Builder(this) // FragmentではActivityを取得して生成
                                            .setTitle("確認")
                                            .setMessage("このIDは他の端末で登録されています。")
                                            .setPositiveButton("OK", { dialog, which ->
                                                // TODO:Yesが押された時の挙動


                                            })
                                            .show()

                                    }
                                }
                                else
                                {
                                    setDeviceID(deviceId,device_token!!)
                                    runOnUiThread {
                                        AlertDialog.Builder(this) // FragmentではActivityを取得して生成
                                            .setTitle("ようこそ!")
                                            .setMessage("建物名:" + json["name"].toString() + "\n部屋:" + json["room_no"].toString())
                                            .setPositiveButton("OK", { dialog, which ->
                                                // TODO:Yesが押された時の挙動

                                                val property_id = json["property_id"].toString()
                                                editor.putBoolean("isLogin",true)
                                                editor.putString("buildingName",json["name"].toString())
                                                editor.putString("room_no",json["room_no"].toString())
                                                editor.putString("id",json["user_id"].toString())
                                                editor.putString("tel",json["tel"].toString())
                                                editor.putString("user_name",json["user_name"].toString())
                                                editor.putString("property_id",json["property_id"].toString())
                                                editor.putString("isMantion2",
                                                    json["isMantion"].toString()
                                                )

                                                Log.d("isMantion",
                                                    json["isMantion"].toString().toBoolean().toString()
                                                )
                                                editor.apply()
                                                val intent = Intent(application,MainActivity::class.java)
                                                startActivity(intent)
                                            })
                                            .show()
                                    }
                                }
                            }

                        }




                    }
                    else if(json["status"] == 1)
                    {
                        runOnUiThread {
                            val dialog = AlertDialog.Builder(this)
                                .setTitle("エラー") // タイトル
                                .setMessage("利用者IDまたはパスワードが間違っています。") // メッセージ
                                .setPositiveButton("閉じる") { dialog, which -> // OK
                                }
                                .create()
                            // AlertDialogを表示
                            dialog.show()
                        }

                    }
                    Log.d("json_res",json["user_pass"].toString())

                }

                is Result.Failure->{
                    print("通信に失敗しました。")
                }
            }
        }

    }
}