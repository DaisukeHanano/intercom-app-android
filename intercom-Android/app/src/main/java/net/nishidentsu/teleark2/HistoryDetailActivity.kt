package net.nishidentsu.teleark2

import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.MenuItem
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import com.github.kittinunf.fuel.httpPost
import com.github.kittinunf.fuel.json.responseJson
import com.github.kittinunf.result.Result
import kotlinx.android.synthetic.main.activity_history_detail.*
import org.json.JSONArray
import org.json.JSONObject

class HistoryDetailActivity : AppCompatActivity() {

    val get_history_url = "https://webrtc.nishi-dentsu.com/teleark/api/GetHistory.php"
    val get_mantion_history_url = "https://webrtc.nishi-dentsu.com/teleark/api/GetMantionHistory.php"


    var jsonAry : JSONArray? = JSONArray()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_history_detail)

        val pref = getSharedPreferences("DataStore", Context.MODE_PRIVATE)
        val id = pref.getString("id","")
        val property_id = intent.getStringExtra("property_id")
        val title = intent.getStringExtra("name")
        supportActionBar!!.title = title
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        val room_no =  pref.getString("room_no","")


        if(pref.getString("isMantion2","0") == "1")
        {
            get_mantion_history_url.httpPost(listOf("room_no" to room_no, "user_id" to id,"property_id" to property_id)).responseJson { request, response, result ->
                when (result) {
                    is Result.Success -> {
                        jsonAry = result.value.array()

                        Log.d("aaa",jsonAry!!.length().toString())

                        runOnUiThread {
                            HistoryRecyclerView.adapter = RecyclerAdapter(this,jsonAry!!)
                            HistoryRecyclerView.layoutManager = LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false)

                        }



                    }

                    is Result.Failure -> {

                    }
                }
            }
        }
        else
        {
            get_history_url.httpPost(listOf("user_id" to id,"property_id" to property_id)).responseJson { request, response, result ->
                when (result) {
                    is Result.Success -> {
                        jsonAry = result.value.array()

                        Log.d("aaa",jsonAry!!.length().toString())

                        runOnUiThread {
                            HistoryRecyclerView.adapter = RecyclerAdapter(this,jsonAry!!)
                            HistoryRecyclerView.layoutManager = LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false)

                        }



                    }

                    is Result.Failure -> {

                    }
                }
            }
        }



    }

    override fun onResume() {
        super.onResume()



    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if(item.itemId == android.R.id.home)
        {
            finish()
            return  true
        }
        return super.onOptionsItemSelected(item)

    }
}