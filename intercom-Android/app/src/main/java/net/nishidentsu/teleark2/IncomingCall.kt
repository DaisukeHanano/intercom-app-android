package net.nishidentsu.teleark2

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.telephony.PhoneStateListener
import android.telephony.TelephonyManager
import android.util.Log
import android.widget.Toast
import androidx.localbroadcastmanager.content.LocalBroadcastManager


/**
 * 着信イベントを受け取る
 * Created by uramotomasaki on 15/06/03.
 * Copyright (c) 2015 Storyboard All Right Reserved.
 */
class IncomingCall : BroadcastReceiver() {
    private val TAG = javaClass.simpleName
    private var ctx: Context? = null
    override fun onReceive(context: Context, intent: Intent) {
        ctx = context
        try {
            //TelephonyManagerの生成
            val tm =
                context.getSystemService(Context.TELEPHONY_SERVICE) as TelephonyManager
            //リスナーの登録
            val PhoneListener =
                MyPhoneStateListener()
            tm.listen(PhoneListener, PhoneStateListener.LISTEN_CALL_STATE)
        } catch (e: Exception) {
            Log.e(TAG, ":$e")
        }
    }

    /**
     * カスタムリスナーの登録
     * 着信〜終了 CALL_STATE_RINGING > CALL_STATE_OFFHOOK > CALL_STATE_IDLE
     * 不在着信 CALL_STATE_RINGING > CALL_STATE_IDLE
     */
    private inner class MyPhoneStateListener : PhoneStateListener() {
        private val TAG = javaClass.simpleName
        override fun onCallStateChanged(state: Int, callNumber: String) {
            Log.d(TAG, ":$state-PhoneNumber:$callNumber")
            when (state) {
                //切断
                TelephonyManager.CALL_STATE_IDLE ->
                {
                    val intent = Intent("GaisenSetudanEvent")
                    LocalBroadcastManager.getInstance(ctx!!).sendBroadcast(intent)
                    Toast.makeText(
                        ctx,
                        "CALL_STATE_IDLE",
                        Toast.LENGTH_LONG
                    ).show()
                }
                //着信中
                TelephonyManager.CALL_STATE_RINGING -> {
                    Toast.makeText(
                        ctx,
                        "CALL_STATE_RINGING: $callNumber",
                        Toast.LENGTH_LONG
                    ).show()
                }
                //応答
                TelephonyManager.CALL_STATE_OFFHOOK -> {
                    val intent = Intent("GaisenOutouEvent")
                    LocalBroadcastManager.getInstance(ctx!!).sendBroadcast(intent)
                    Toast.makeText(
                        ctx,
                        "CALL_STATE_OFFHOOK",
                        Toast.LENGTH_LONG
                    ).show()
                }
            }
        }
    }
}