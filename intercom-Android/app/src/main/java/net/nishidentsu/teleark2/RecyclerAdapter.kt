package net.nishidentsu.teleark2

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import org.json.JSONArray
import org.json.JSONObject

class RecyclerAdapter(private val context: Context,  private val itemList:JSONArray) : RecyclerView.Adapter<RecyclerAdapter.HistoryViewHolder>()  {

    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): HistoryViewHolder {
        val v = LayoutInflater.from(context).inflate(R.layout.list_item, viewGroup, false)
        return HistoryViewHolder(v)
    }

    class HistoryViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val historyImage: ImageView = view.findViewById(R.id.historyImage)

        val HistoryDateTV: TextView = view.findViewById(R.id.HistoryDateTV)
        val HistoryContentTV: TextView = view.findViewById(R.id.HistoryContentTV)
    }

    override fun getItemCount(): Int {
        return itemList.length()
    }

    override fun onBindViewHolder(holder: HistoryViewHolder, position: Int) {

        val history : JSONObject = itemList.get(position) as JSONObject
        holder.let {

            it.HistoryDateTV.text = history["update_date"] as String
            it.HistoryContentTV.text = history["content"] as String
            it.historyImage.setImageBitmap(null)
            if(history["image_url"] != null && history["image_url"] != "")
            {
                Picasso.get()
                    .load(history["image_url"] as String)
                    .into(it.historyImage)
            }

        }



    }
}