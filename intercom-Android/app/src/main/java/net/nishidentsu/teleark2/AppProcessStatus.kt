package net.nishidentsu.teleark2

import android.app.ActivityManager
import android.content.Context
import android.os.Build

/**
 * アプリのプロセスの状態
 */
enum class AppProcessStatus {
    /** 最前面で起動中 */
    FOREGROUND,
    /** バックグラウンド状態 */
    BACKGROUND,
    /** プロセスが存在しない */
    GONE;

    companion object {
        /** 現在のアプリのプロセスの状態を取得する */
        fun current(context: Context): AppProcessStatus {
            if (!existsAppTask(context)) return GONE
            if (isForeground(context)) return FOREGROUND
            return BACKGROUND
        }

        private fun isForeground(context: Context): Boolean {
            val am = context.getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager
            val runningProcesses = am.runningAppProcesses
            for (processInfo in runningProcesses) {
                for (activeProcess in processInfo.pkgList) {
                    if (processInfo.importance == ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND) {
                        return true
                    }
                }
            }
            return false
        }

        private fun existsAppTask(context: Context): Boolean {
            val am = context.getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                return am.appTasks.count() > 0
            }
            val recentTasks = am.getRunningTasks(Integer.MAX_VALUE)
            for (i in recentTasks.indices) {
                if (recentTasks[i].baseActivity!!.packageName == context.packageName) {
                    return true
                }
            }
            return false
        }

        
    }
}