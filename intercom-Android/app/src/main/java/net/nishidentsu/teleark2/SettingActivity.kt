package net.nishidentsu.teleark2


import android.Manifest
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.media.AudioManager
import android.media.MediaPlayer
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.telephony.TelephonyManager
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import io.skyway.BuildConfig
import kotlinx.android.synthetic.main.activity_setting.*
import java.io.IOException


class SettingActivity : AppCompatActivity(){


    var SoundType = 0

    var mediaPlayer:MediaPlayer? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_setting)


        val pref = getSharedPreferences("DataStore", Context.MODE_PRIVATE)

        SoundType = pref.getInt("SoundType",0)

        soundTV.text = "着信音" + (SoundType + 1).toString()
        supportActionBar!!.title = ""

    }


    private fun audioSetup(): Boolean {
        var fileCheck = false

        // インタンスを生成
        mediaPlayer = MediaPlayer()

        //音楽ファイル名, あるいはパス
        val filePath = "sound" + (SoundType+1).toString() + ".mp3"

        // assetsから mp3 ファイルを読み込み
        try {
            assets.openFd(filePath).use { afdescripter ->
                // MediaPlayerに読み込んだ音楽ファイルを指定
                mediaPlayer!!.setDataSource(
                    afdescripter.fileDescriptor,
                    afdescripter.startOffset,
                    afdescripter.length
                )
                // 音量調整を端末のボタンに任せる
                volumeControlStream = AudioManager.STREAM_MUSIC
                mediaPlayer!!.prepare()
                fileCheck = true
            }
        } catch (e1: IOException) {
            e1.printStackTrace()
        }
        return fileCheck
    }

    private fun audioPlay() {
        if (mediaPlayer == null) {
            // audio ファイルを読出し
            if (audioSetup()) {
//                Toast.makeText(application, "Rread audio file", Toast.LENGTH_SHORT).show()
            } else {
                Toast.makeText(application, "Error: read audio file", Toast.LENGTH_SHORT)
                    .show()
                return
            }
        } else {
            // 繰り返し再生する場合
            mediaPlayer!!.stop()
            mediaPlayer!!.reset()
            // リソースの解放
            mediaPlayer!!.release()
        }

        // 再生する
        mediaPlayer!!.start()

        // 終了を検知するリスナー
        mediaPlayer!!.setOnCompletionListener {
            Log.d("debug", "end of audio")
            audioStop()
        }
    }

    private fun audioStop() {
        if(mediaPlayer != null)
        {
            // 再生終了
            mediaPlayer!!.stop()
            // リセット
            mediaPlayer!!.reset()
            // リソースの解放
            mediaPlayer!!.release()
            mediaPlayer = null
        }

    }


    override fun onPause() {
        super.onPause()
        val pref = getSharedPreferences("DataStore", Context.MODE_PRIVATE)

        val editor = pref.edit()
        editor.putInt("SoundType" , SoundType)
        editor.apply()
    }

    public fun ReportTapped(view: View) {

        val pref = getSharedPreferences("DataStore", Context.MODE_PRIVATE)
        val buildingName = pref.getString("buildingName","")
        val room_no = pref.getString("room_no","")
        val id = pref.getString("id","")
        var tel = ""
        val telManager = getSystemService(Context.TELEPHONY_SERVICE) as TelephonyManager
        if (ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.READ_SMS
            ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.READ_PHONE_NUMBERS
            ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.READ_PHONE_STATE
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return
        }
        if(telManager.line1Number != null)
        {
            tel = telManager.line1Number
        }
        else{
            tel = ""
        }



        val model = Build.MODEL
        val os = Build.VERSION.RELEASE
        val addressees = arrayOf("nishidentsu001@gmail.com")
        val versionName : String = BuildConfig.VERSION_NAME
        val intent = Intent(Intent.ACTION_SENDTO)
        intent.setData(Uri.parse("mailto:"))
        intent.putExtra(Intent.EXTRA_EMAIL,addressees)
        intent.putExtra(Intent.EXTRA_SUBJECT,"問題を報告")
        intent.putExtra(Intent.EXTRA_TEXT, "不具合の内容を入力してから送信ボタンをタップして下さい。（字数制限はありません。）\n" +
                "-----ここから------\n" +
                "\n" +
                "\n" +
                "\n" +
                "-----ここまで------\n" +
                "下記の情報を消さないで送信して下さい。\n" +
                "建物名:$buildingName\n" +
                "部屋番号:$room_no\n" +
                "利用者id:$id\n" +
                "電話番号:$tel\n" +
                "System Info:$model\u000BOS:$os\u000BApp Version:$versionName\n");

        if (intent.resolveActivity(packageManager) != null) {
            startActivity(intent);
        }
    }

    public fun PrivacyTapped(view: View) {
        val uri = Uri.parse("http://www.nishi-dentsu.co.jp/date/riyoukiyaku.pdf")
        val intent = Intent(Intent.ACTION_VIEW,uri)
        startActivity(intent)
    }

    public fun MusicTapped(view: View) {
        val menus = arrayOf("着信音1","着信音2","着信音3","着信音4","着信音5")
        AlertDialog.Builder(this) // FragmentではActivityを取得して生成
            .setTitle("確認")
            .setCancelable(false)
            .setSingleChoiceItems(menus, SoundType,{ dialog, which ->
                SoundType = which
                audioStop()
                audioPlay()
                if(which == 0)
                {
                    print("着信音1")
                }
                else if(which == 1)
                {
                    print("着信音2")
                }
                else if(which == 2)
                {
                    print("着信音3")
                }
                else if(which == 3)
                {
                    print("着信音4")
                }
                else if(which == 4)
                {
                    print("着信音5")
                }
            })
            .setPositiveButton("OK", { dialog, which ->
                // TODO:Yesが押された時の挙動
                audioStop()
                soundTV.text = "着信音" + (SoundType + 1).toString()

            })

            .show()
    }



}