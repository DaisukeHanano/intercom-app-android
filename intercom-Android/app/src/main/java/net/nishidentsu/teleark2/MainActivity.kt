package net.nishidentsu.teleark2

import android.Manifest
import android.annotation.SuppressLint
import android.app.*
import android.content.*
import android.content.pm.PackageManager
import android.graphics.Color
import android.media.AudioDeviceInfo
import android.media.AudioManager
import android.media.MediaPlayer
import android.net.Uri
import android.os.*
import android.provider.Settings
import android.util.Log
import android.view.*
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.github.kittinunf.fuel.httpPost
import com.github.kittinunf.fuel.json.responseJson
import com.github.kittinunf.result.Result
import io.skyway.Peer.*
import io.skyway.Peer.Browser.Canvas
import io.skyway.Peer.Browser.MediaConstraints
import io.skyway.Peer.Browser.MediaStream
import io.skyway.Peer.Browser.Navigator
import kotlinx.android.synthetic.main.activity_main.*
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*
import kotlin.concurrent.schedule


class MainActivity : AppCompatActivity() {



    private var keyguard: KeyguardManager? = null
    private var keylock: KeyguardManager.KeyguardLock? = null
    private var wakelock: PowerManager.WakeLock? = null
    var option:PeerOption? = null
    val API_KEY = "5d26dae7-6e58-43eb-b22b-829fbe4c6cab"
    val DOMAIN = "localhost"

    var pushURL = "https://webrtc.nishi-dentsu.com/teleark/api/push/send_android.php"
    var pushURL_ios = "https://webrtc.nishi-dentsu.com/teleark/api/push/send_ios.php"

    var pushURL_Mantion = "https://webrtc.nishi-dentsu.com/teleark/api/push/send_mansion_android.php"
    var pushURL_Mantion_ios = "https://webrtc.nishi-dentsu.com/teleark/api/push/send_mansion_ios.php"

    val set_history_url = "https://webrtc.nishi-dentsu.com/teleark/api/SetHistory.php"
    val set_mantion_history_url = "https://webrtc.nishi-dentsu.com/teleark/api/SetMantionHistory.php"

    var EntranceImageURL = ""

    var peer : Peer? = null
    var localStream : MediaStream? = null
    var remoteStream : MediaStream? = null
    var strOwnId : String = ""
    var bConnected = false
    var handler : Handler? = null
    var mediaConnection : MediaConnection? = null
    var dataConnection: DataConnection? = null
    var isMantion = false
    var isToilet = false
    var dataConnection2:DataConnection? = null
    var vibrator : Vibrator? = null
    var chakushinName = ""
    var isOutou = false
    var isChakushin = false
    var SoundType = 0
    var mediaPlayer:MediaPlayer? = null
    var m_ChakushinDialog : AlertDialog? = null
    var TelMAXTime = 30000
    var MaxTimecallHandler:Handler? = null //最大通話時間用のタイマー
    var Dataconnections = emptyArray<DataConnection>()


    private val mMessageReceiver: BroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent) {
            // Extraから値を得る
            if(!isOutou!!)
            {
                isOutou = false
                val pref = getSharedPreferences("DataStore", Context.MODE_PRIVATE)
                val editor = pref.edit()
                editor.putBoolean("isOutou", isOutou)
                isChakushin = false
                EntranceImageURL = ""
                editor.putBoolean("isChakushin", isChakushin)



                editor.apply()

                disconnect3()
                vibrator!!.cancel()
                audioStop()
                closeRemoteStream3()

                if(m_ChakushinDialog != null)
                {
                    m_ChakushinDialog!!.dismiss()
                    m_ChakushinDialog == null
                }
                Toast.makeText(this@MainActivity, "他の室内機で応答しました。", Toast.LENGTH_LONG).show()
            }

        }
    }

    private val mMessageReceiver2: BroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent) {
            // Extraから値を得る
            if(!isOutou!!)
            {

                isOutou = false
                val pref = getSharedPreferences("DataStore", Context.MODE_PRIVATE)
                val editor = pref.edit()
                editor.putBoolean("isOutou", isOutou)
                isChakushin = false
                editor.putBoolean("isChakushin", isChakushin)

                val text = pref.getString("pushMsg", "")
                setHistoryLog(text!!, EntranceImageURL)
                Toast.makeText(this@MainActivity, "他の室内機で解錠しました。", Toast.LENGTH_LONG).show()
            }

        }
    }



    private val OutouReceiver: BroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent) {
            // Extraから値を得る
            if(isOutou!!)
            {
                localStream!!.setEnableAudioTrack(0, false)
                val str = "外線応答"
                dataConnection!!.send(str)
            }

        }
    }

    private val SetudanReceiver: BroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent) {
            // Extraから値を得る
            if(isOutou!!)
            {


                val str = "外線切断"
                dataConnection!!.send(str)
                localStream!!.setEnableAudioTrack(0, true)
//                Handler().postDelayed(Runnable {
//                    localStream!!.setEnableAudioTrack(0,true)
//                }, 1000)
            }

        }
    }

    fun setAlarm(context: Context) {
        // 時間をセットする
        val calendar = Calendar.getInstance()
        // Calendarを使って現在の時間をミリ秒で取得
        calendar.timeInMillis = System.currentTimeMillis()
        // 5秒後に設定
        calendar.add(Calendar.SECOND, 5)

        //明示的なBroadCast
        val intent = Intent(
            applicationContext,
            AlarmReceiver::class.java
        )
        val pendingIntent = PendingIntent.getBroadcast(
            applicationContext, 1, intent, 0
        )

        //アラームクロックインフォを作成してアラーム時間をセット
        val clockInfo = AlarmManager.AlarmClockInfo(calendar.timeInMillis, null)
        //アラームマネージャーにアラームクロックをセット
        (context.getSystemService(Context.ALARM_SERVICE) as AlarmManager).setAlarmClock(clockInfo, pendingIntent)
    }

    @SuppressLint("InvalidWakeLockTag")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val pref = getSharedPreferences("DataStore", Context.MODE_PRIVATE)


        Timer().schedule(4000, 20000) {
            runOnUiThread {

                setAlarm(this@MainActivity)
                wakelock = (getSystemService(Context.POWER_SERVICE) as PowerManager)
                    .newWakeLock(
                        PowerManager.FULL_WAKE_LOCK
                                or PowerManager.ACQUIRE_CAUSES_WAKEUP
                                or PowerManager.ON_AFTER_RELEASE, "disableLock"
                    )
                wakelock!!.acquire(20);
                keyguard = getSystemService(android.content.Context.KEYGUARD_SERVICE) as KeyguardManager
                keylock = keyguard!!.newKeyguardLock("disableLock");
                keylock!!.disableKeyguard();


                if (Build.VERSION.SDK_INT >= 26) {
                    val requestCode = 0
                    val channelId = "default"
                    val title: String = getString(R.string.app_name)
                    val pendingIntent = PendingIntent.getActivity(
                        this@MainActivity, requestCode,
                        intent, PendingIntent.FLAG_UPDATE_CURRENT
                    )

                    // ForegroundにするためNotificationが必要、Contextを設定
                    val notificationManager:NotificationManager = this@MainActivity.getSystemService(NOTIFICATION_SERVICE) as NotificationManager

                    // Notification　Channel 設定
                    val channel = NotificationChannel(
                        channelId, title, NotificationManager.IMPORTANCE_DEFAULT
                    )
                    channel.description = "Silent Notification"
                    // 通知音を消さないと毎回通知音が出てしまう
                    // この辺りの設定はcleanにしてから変更
                    channel.setSound(null, null)
                    // 通知ランプを消す
                    channel.enableLights(false)
                    channel.lightColor = Color.BLUE
                    // 通知バイブレーション無し
                    channel.enableVibration(false)
                    if (notificationManager != null) {
                        notificationManager.createNotificationChannel(channel)
                        val notification = Notification.Builder(this@MainActivity, channelId)
                            .setContentTitle(title) // 本来なら衛星のアイコンですがandroid標準アイコンを設定
                            .setSmallIcon(android.R.drawable.btn_star)
                            .setContentText("接続維持動作")
                            .setAutoCancel(true)
                            .setContentIntent(pendingIntent)

                            .setWhen(System.currentTimeMillis())
                            .build()

                        notification.flags = Notification.FLAG_NO_CLEAR
                        notificationManager.notify(1,notification)
                        // startForeground
//                startForeground(1, notification)
                    }
                }
            }


        }
                //終了
                //this.cancel()


        //---------------------------------------------------


        window.addFlags(
            WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED or
                    WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD or
                    WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON or
                    WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON or
                    WindowManager.LayoutParams.FLAG_ALLOW_LOCK_WHILE_SCREEN_ON
        )
//        Timer().scheduleAtFixedRate(object : TimerTask() {
//            override fun run() {
//                wakelock = (getSystemService(Context.POWER_SERVICE) as PowerManager)
//                    .newWakeLock(
//                        PowerManager.FULL_WAKE_LOCK
//                                or PowerManager.ACQUIRE_CAUSES_WAKEUP
//                                or PowerManager.ON_AFTER_RELEASE, "disableLock"
//                    )
//                wakelock!!.acquire(10)
//
//
//            }
//        }, 0, 10000)

        if(pref.getString("isMantion2", "0") == "1")
        {
            CallEntranceBtn.visibility = View.INVISIBLE
            isMantion = true
        }
        supportActionBar!!.title = ""
        option = PeerOption()
        option!!.key = API_KEY
        option!!.domain = DOMAIN
        option!!.debug = Peer.DebugLevelEnum.ALL_LOGS
        peer = Peer(this, pref.getString("deviceid", ""), option)

        LocalBroadcastManager.getInstance(this).registerReceiver(
            mMessageReceiver,
            IntentFilter("OutouEvent")
        )
        LocalBroadcastManager.getInstance(this).registerReceiver(
            mMessageReceiver2,
            IntentFilter("KaijyouEvent")
        )
        LocalBroadcastManager.getInstance(this).registerReceiver(
            OutouReceiver,
            IntentFilter("GaisenOutouEvent")
        )
        LocalBroadcastManager.getInstance(this).registerReceiver(
            SetudanReceiver,
            IntentFilter("GaisenSetudanEvent")
        )


        val powerManager : PowerManager = getSystemService(Context.POWER_SERVICE) as PowerManager
        if(!powerManager.isIgnoringBatteryOptimizations(packageName))
        {
            val intent = Intent(Settings.ACTION_REQUEST_IGNORE_BATTERY_OPTIMIZATIONS)
            intent.setData(Uri.parse("package:" + packageName))
            startActivity(intent)
        }



    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(data != null)
        {
            val bundle = data!!.extras
            if(requestCode == 999)
            {

                bundle!!.getString("peer_id")?.let { call(it) }
            }
        }

    }

    override fun onStart() {
        super.onStart()
    }

    fun call(peerID: String)
    {
        localStream!!.setEnableAudioTrack(0, true)//Do Something
        dataConnection = peer!!.connect(peerID)
        val option: CallOption = CallOption()
        mediaConnection = peer!!.call(peerID, localStream, option)

        setMediaCallbacks()

        CallEntranceBtn.isEnabled = false
        historyBtn.isEnabled = false

        Handler().postDelayed(Runnable {
            dataConnection!!.send("応答")
            waitTV.visibility = View.GONE
            opendoor.visibility = View.VISIBLE
            rejectButton.visibility = View.VISIBLE
            spacer.visibility = View.VISIBLE
            bConnected = true
        }, 2000)

    }

    override fun onResume() {
        super.onResume()
        val pref = getSharedPreferences("DataStore", Context.MODE_PRIVATE)


        setAudioOutput(AudioOutput.Speaker)
//        setAudioOutput(AudioOutput.Call)

        setSkywayCallback()
        val permission = ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA)
        val permission2 = ContextCompat.checkSelfPermission(this, Manifest.permission.RECORD_AUDIO)
        if (permission != PackageManager.PERMISSION_GRANTED && permission2 != PackageManager.PERMISSION_GRANTED) {
            requestPermission()

        }
        else{
            startLocalStream()
        }

        vibrator = getSystemService(Context.VIBRATOR_SERVICE) as Vibrator

        SoundType = pref.getInt("SoundType", 0)
        if(pref.getBoolean("isChakushin", false))
        {

            val editor = pref.edit()
            editor.putBoolean("isChakushin", false)
            editor.apply()

            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
                val vibrationEffect: VibrationEffect = VibrationEffect.createWaveform(
                    longArrayOf(
                        200,
                        200
                    ), 1
                )
                vibrator!!.vibrate(vibrationEffect)
            } else {
                vibrator!!.vibrate(5000)
            }

            audioPlay()
        }


        opendoor.setOnClickListener {
            val str = "解錠"
            var bResult = false

            if(dataConnection != null)
            {

                svRemoteView.visibility = View.VISIBLE
                ToiletTextView.text = ""

                try {
                    localStream!!.setEnableAudioTrack(0, false)//Do Something
                    if (dataConnection != null) {
                        bResult = dataConnection!!.send(str)
                    }

                }catch(e: Exception){
                    println(e)
                }

                setHistoryLog("応答し解錠しました", EntranceImageURL)
                EntranceImageURL = ""
                kaijyouPush()
                isChakushin = false
                isOutou = false
                historyBtn.isEnabled = true
                CallEntranceBtn.isEnabled = true
//                isMantion = false
                isToilet = false
                if(MaxTimecallHandler != null)
                {
                    MaxTimecallHandler!!.removeCallbacksAndMessages(null)
                }
                val pref = getSharedPreferences("DataStore", Context.MODE_PRIVATE)
                val editor = pref.edit()
                editor.putBoolean("isMantion", isMantion)
                editor.apply()
                waitTV.visibility = View.GONE
                opendoor.visibility = View.GONE
                rejectButton.visibility = View.GONE
                spacer.visibility = View.GONE
                Handler().postDelayed(Runnable {
                    disconnect2()
                }, 2000)
                print(bResult)
                runOnUiThread {

//                    var kaijyouDialog = AlertDialog.Builder(this@MainActivity) // FragmentではActivityを取得して生成
//                        .setTitle("確認")
//                        .setMessage("解錠しました。")
//                        .setCancelable(false)
//                        .setPositiveButton("OK", { dialog, which ->
//                            // TODO:Yesが押された時の挙動
//                            Handler().postDelayed(Runnable {
//                                this.finish()
//                                System.exit(0)
////                                this.moveTaskToBack(true);
//                            }, 3500)
//
//                        })
//                        .show()
//                    kaijyouDialog.getButton(AlertDialog.BUTTON_POSITIVE).textSize = 26.0f


                    Toast.makeText(this@MainActivity, "解錠しました", Toast.LENGTH_LONG).show()

                    Handler().postDelayed(Runnable {
//                                this.finish()
//                                System.exit(0)
//                        this.finish();
                        this.moveTaskToBack(true);
                        waitTV.visibility = View.VISIBLE
//                                this.moveTaskToBack(true);
                    }, 3500)
                }
            }
            else{
                runOnUiThread {
                    var DG =  AlertDialog.Builder(this@MainActivity) // FragmentではActivityを取得して生成
                        .setTitle("確認")
                        .setMessage("玄関機と接続されていないため解錠できません。")
                        .setPositiveButton("OK", { dialog, which ->
                            // TODO:Yesが押された時の挙動

                        })
                        .show()
                    DG.getButton(AlertDialog.BUTTON_POSITIVE).textSize = 26.0f
                }
            }

        }
    }

    fun disconnect()
    {
        //電話を切断するメソッド



        val str = "最大通話時間超過"
        var bResult = false
        try {
            if (dataConnection != null) {
                bResult = dataConnection!!.send(str)
            }

        }catch(e: Exception){
            println(e)
        }


        svRemoteView.visibility = View.VISIBLE
        Handler().postDelayed(Runnable {

            isOutou = false
            historyBtn.isEnabled = true
            CallEntranceBtn.isEnabled = true
            waitTV.visibility = View.VISIBLE
            opendoor.visibility = View.GONE
            rejectButton.visibility = View.GONE
            spacer.visibility = View.GONE
            val pref = getSharedPreferences("DataStore", Context.MODE_PRIVATE)

            val editor = pref.edit()
            editor.putBoolean("isOutou", isOutou)
            editor.apply()
            if (!bConnected) {

            } else {
                closeRemoteStream()
                mediaConnection!!.close()
                dataConnection!!.close()
                mediaConnection = null
                dataConnection = null
            }
        }, 2000)

    }

    fun disconnect2()
    {
        //電話を切断するメソッド

        isOutou = false
        historyBtn.isEnabled = true
        CallEntranceBtn.isEnabled = true
//        isMantion = false
        isToilet = false
        val pref = getSharedPreferences("DataStore", Context.MODE_PRIVATE)

        waitTV.visibility = View.VISIBLE
        opendoor.visibility = View.GONE
        rejectButton.visibility = View.GONE
        spacer.visibility = View.GONE
        val editor = pref.edit()
        editor.putBoolean("isOutou", isOutou)

        editor.putBoolean("isMantion", isMantion)
        editor.apply()
        if(!bConnected)
        {

        }
        else
        {
            closeRemoteStream2()

        }
    }


    fun disconnect3()
    {
        //電話を切断するメソッド

        isOutou = false
        historyBtn.isEnabled = true
        CallEntranceBtn.isEnabled = true
//        isMantion = false
        isToilet = false
        val pref = getSharedPreferences("DataStore", Context.MODE_PRIVATE)

        waitTV.visibility = View.VISIBLE
        opendoor.visibility = View.GONE
        rejectButton.visibility = View.GONE
        spacer.visibility = View.GONE
        val editor = pref.edit()
        editor.putBoolean("isOutou", isOutou)

        editor.putBoolean("isMantion", isMantion)
        editor.apply()
        if(!bConnected)
        {

        }
        else
        {
            closeRemoteStream3()

        }
    }

    fun startLocalStream(){
        Navigator.initialize(peer)
        val constraints = MediaConstraints()
        constraints.cameraPosition = MediaConstraints.CameraPositionEnum.FRONT
        localStream = Navigator.getUserMedia(constraints)
//        val canvas = findViewById(R.id.svLocalView) as Canvas


        localStream!!.addVideoRenderer(null, 0)
    }



    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.option_menu, menu)

        return super.onCreateOptionsMenu(menu)
    }

    fun GoSetting(){
        val intent = Intent(application, SettingActivity::class.java)
        startActivity(intent)
    }

    fun GoMyInfoSetting(){
        val intent = Intent(application, MyInfoActivity::class.java)

        startActivity(intent)
    }



    override fun onOptionsItemSelected(item: MenuItem): Boolean {
//        switch (item.itemId){
//
//        }
        when (item.itemId)
        {
            R.id.item1 -> GoSetting()
            R.id.item2 -> GoMyInfoSetting()
        }
        return super.onOptionsItemSelected(item)
    }


    private fun requestPermission() {
        requestPermissions(
            arrayOf(Manifest.permission.CAMERA, Manifest.permission.RECORD_AUDIO),
            200
        )
    }

    enum class AudioOutput{
        Speaker,
        Call
    }

    fun setAudioOutput(type: AudioOutput?){
        val audioManager = getSystemService(Context.AUDIO_SERVICE) as? AudioManager
        val mediaPlayer = MediaPlayer()

        // MODE_IN_COMMUNICATION: In communication audio mode. An audio/video chat or VoIP call is established.
        audioManager?.mode = AudioManager.MODE_IN_COMMUNICATION

        type?.let{
            // Adjust output for Speakerphone.
            audioManager?.isSpeakerphoneOn = when(it){
                AudioOutput.Call -> false
                AudioOutput.Speaker -> true
            }
        } ?: run{
            if(this.isHeadsetOn()){
                audioManager?.isSpeakerphoneOn = false
            }else{
                audioManager?.isSpeakerphoneOn = true
            }
        }

        mediaPlayer.setAudioStreamType(AudioManager.MODE_IN_COMMUNICATION)
    }

    fun isHeadsetOn(): Boolean {
        val audioManager = getSystemService(Context.AUDIO_SERVICE) as AudioManager
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            return audioManager.isWiredHeadsetOn() || audioManager.isBluetoothA2dpOn()
        } else {
            val devices = audioManager.getDevices(AudioManager.GET_DEVICES_OUTPUTS)
            for (i in devices.indices) {
                val device = devices[i]
                if (device.type === AudioDeviceInfo.TYPE_WIRED_HEADPHONES
                    || device.type === AudioDeviceInfo.TYPE_BLUETOOTH_A2DP
                    || device.type === AudioDeviceInfo.TYPE_BLUETOOTH_SCO) {
                    return true
                }
            }
        }
        return false
    }



    fun setMediaCallbacks(){

        mediaConnection!!.on(MediaConnection.MediaEventEnum.STREAM, OnCallback {
            runOnUiThread {
                remoteStream = it as MediaStream
                val canvas = svRemoteView as Canvas
                remoteStream!!.addVideoRenderer(canvas, 0)
            }


        })

        mediaConnection!!.on(MediaConnection.MediaEventEnum.CLOSE, OnCallback {
            bConnected = false
//            closeRemoteStream()

            CallEntranceBtn.isEnabled = false
            historyBtn.isEnabled = false

        })
    }

    public fun HistoryButtonTapped(view: View)
    {
        val intent = Intent(application, HistoryActivity::class.java)
        startActivity(intent)
    }

    public fun EntranceCallButtonTapped(view: View)
    {
        val intent = Intent(application, CallEntranceActivity::class.java)
//        startActivity(intent)
        startActivityForResult(intent, 999)
    }


    public fun disconnectButtonTapped(view: View) {


        if(dataConnection != null)
        {
            val str = "切断"

            svRemoteView.visibility = View.VISIBLE
            ToiletTextView.text = ""
            setHistoryLog("切断しました", EntranceImageURL)
            EntranceImageURL = ""
            var bResult = false

            try {
                localStream!!.setEnableAudioTrack(0, false)//Do Something
                if (dataConnection != null) {
                    bResult = dataConnection!!.send(str)
                }

            }catch(e: Exception){
                println(e)
            }
            isChakushin = false
            isOutou = false
//            isMantion = false
            historyBtn.isEnabled = true
            CallEntranceBtn.isEnabled = true
            isToilet = false

            val pref = getSharedPreferences("DataStore", Context.MODE_PRIVATE)
            val editor = pref.edit()
            editor.putBoolean("isMantion", isMantion)
            editor.apply()
            waitTV.visibility = View.GONE
            opendoor.visibility = View.GONE
            rejectButton.visibility = View.GONE
            spacer.visibility = View.GONE

            if(MaxTimecallHandler != null)
            {
                MaxTimecallHandler!!.removeCallbacksAndMessages(null)
            }


            Handler().postDelayed(Runnable {
                if (!bConnected) {

                } else {
                    closeRemoteStream()

                }
            }, 2000)


            runOnUiThread {


                Toast.makeText(this@MainActivity, "切断しました", Toast.LENGTH_LONG).show()

                Handler().postDelayed(Runnable {
//                    this.finish()
//                    System.exit(0)
//                    this.finish();
                    this.moveTaskToBack(true);
                    waitTV.visibility = View.VISIBLE
                }, 3500)
            }
        }
        else{
            runOnUiThread {
                var DG = AlertDialog.Builder(this@MainActivity) // FragmentではActivityを取得して生成
                    .setTitle("確認")
                    .setMessage("玄関機に接続されていないため切断できません。")
                    .setPositiveButton("OK", { dialog, which ->
                        // TODO:Yesが押された時の挙動


                    })
                    .show()
                DG.getButton(AlertDialog.BUTTON_POSITIVE).textSize = 26.0f
            }
        }

    }

    private fun audioSetup(): Boolean {
        var fileCheck = false

        // インタンスを生成
        mediaPlayer = MediaPlayer()

        //音楽ファイル名, あるいはパス
        val filePath = "sound" + (SoundType+1).toString() + ".mp3"

        // assetsから mp3 ファイルを読み込み
        try {
            assets.openFd(filePath).use { afdescripter ->
                // MediaPlayerに読み込んだ音楽ファイルを指定
                mediaPlayer!!.setDataSource(
                    afdescripter.fileDescriptor,
                    afdescripter.startOffset,
                    afdescripter.length
                )
                // 音量調整を端末のボタンに任せる
                volumeControlStream = AudioManager.STREAM_RING
                mediaPlayer!!.setAudioStreamType(AudioManager.STREAM_MUSIC)
                mediaPlayer!!.prepare()
                fileCheck = true
            }
        } catch (e1: IOException) {
            e1.printStackTrace()
        }
        return fileCheck
    }

    private fun audioPlay() {
        if (mediaPlayer == null) {
            // audio ファイルを読出し
            if (audioSetup()) {
//                Toast.makeText(application, "Rread audio file", Toast.LENGTH_SHORT).show()
            } else {
                Toast.makeText(application, "Error: read audio file", Toast.LENGTH_SHORT)
                    .show()
                return
            }
        } else {
            try {
                if(!mediaPlayer!!.isPlaying)
                {
                    // 繰り返し再生する場合
                    mediaPlayer!!.stop()
                    mediaPlayer!!.reset()
                    // リソースの解放
                    mediaPlayer!!.release()
                }

            }catch (e: Exception){
                println(e)
            }


        }

        try {

            if(!mediaPlayer!!.isPlaying)
            {
                mediaPlayer!!.isLooping = true
                // 再生する
                mediaPlayer!!.start()

                // 終了を検知するリスナー
                mediaPlayer!!.setOnCompletionListener {
                    Log.d("debug", "end of audio")
                    audioStop()
                }
            }

        }catch (e: Exception){
            println(e)
        }


    }

    private fun audioStop() {
        try {
            if(mediaPlayer != null)
            {
                // 再生終了
                mediaPlayer!!.stop()
                // リセット
                mediaPlayer!!.reset()
                // リソースの解放
                mediaPlayer!!.release()
                mediaPlayer = null
            }
        }catch (e: Exception){
            println(e)
        }



    }

    override fun onUserLeaveHint() {
        super.onUserLeaveHint()
        if(vibrator != null)
        {
            vibrator!!.cancel()
        }


        val pref = getSharedPreferences("DataStore", Context.MODE_PRIVATE)

        val editor = pref.edit()
        editor.putBoolean("isChakushin", false)
        editor.apply()
//        this.moveTaskToBack(true);
//        finishAndRemoveTask()

    }



    override fun onDestroy() {
        super.onDestroy()
        val pref = getSharedPreferences("DataStore", Context.MODE_PRIVATE)

        val editor = pref.edit()
        editor.putBoolean("isChakushin", false)
        editor.apply()

        LocalBroadcastManager.getInstance(this).unregisterReceiver(mMessageReceiver)

    }

    fun setHistoryLog(content: String, url: String)
    {
        val pref = getSharedPreferences("DataStore", Context.MODE_PRIVATE)
        val id = pref.getString("id", "")
        val property_id = pref.getString("ChakushinPropertyID", "")

        val room_no =  pref.getString("room_no", "")
        if(isMantion == true)
        {
            set_mantion_history_url.httpPost(
                listOf(
                    "room_no" to room_no,
                    "user_id" to id,
                    "content" to content,
                    "property_id" to property_id,
                    "url" to url
                )
            ).responseJson { request, response, result ->
                when (result) {
                    is Result.Success -> {


                    }

                    is Result.Failure -> {

                    }
                }
            }
        }
        else
        {
            set_history_url.httpPost(
                listOf(
                    "user_id" to id,
                    "content" to content,
                    "property_id" to property_id,
                    "url" to url
                )
            ).responseJson { request, response, result ->
                when (result) {
                    is Result.Success -> {


                    }

                    is Result.Failure -> {

                    }
                }
            }
        }

    }

    fun closeRemoteStream(){
        if(remoteStream == null)
        {
            return
        }
        
        val canvas = svRemoteView as Canvas



        remoteStream!!.removeVideoRenderer(canvas, 0)
        remoteStream!!.close()

        localStream!!.setEnableAudioTrack(0, false)

        if(dataConnection == null)
        {
            return
        }

        dataConnection!!.close()
        dataConnection = null
    }

    fun closeRemoteStream2(){
        if(remoteStream == null)
        {
            return
        }

        val canvas = svRemoteView as Canvas


        remoteStream!!.removeVideoRenderer(canvas, 0)
        remoteStream!!.close()

        localStream!!.setEnableAudioTrack(0, false)


    }

    fun closeRemoteStream3(){
        if(remoteStream == null)
        {
            return
        }

        val canvas = svRemoteView as Canvas

        remoteStream!!.removeVideoRenderer(canvas, 0)
        remoteStream!!.setEnableAudioTrack(0,false)
        localStream!!.setEnableAudioTrack(0, false)


    }

    override fun dispatchKeyEvent(event: KeyEvent?): Boolean {
        if(event!!.keyCode == KeyEvent.KEYCODE_POWER)
        {
            print("power")
        }
        return super.dispatchKeyEvent(event)
    }

    override fun onBackPressed() {
        this.moveTaskToBack(true)
    }

    private fun moveTaskToFront() {
        val id = getMyAppId()
        if (id > 0) {
            val activityManager =
                getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager
            activityManager.moveTaskToFront(id, ActivityManager.MOVE_TASK_NO_USER_ACTION)
        }
    }

    /**
     * @return 自分のアプリのId
     *
     * マイナス値の場合は自分自身が起動していない
     */
    private fun getMyAppId(): Int {
        val id = -1
        val activityManager =
            getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager
        val recentTasks =
            activityManager.getRunningTasks(Int.MAX_VALUE)
        for (i in recentTasks.indices) {
            if (recentTasks[i].baseActivity!!.packageName == packageName) {
                return recentTasks[i].id
            }
        }
        return id
    }

    @SuppressLint("ResourceAsColor")
    fun setSkywayCallback(){

        peer!!.on(Peer.PeerEventEnum.CALL, OnCallback {
            if (it !is MediaConnection) {
                return@OnCallback
            }

            if (isChakushin == false) {

                moveTaskToFront()
                mediaConnection = it as MediaConnection

                setMediaCallbacks()
                isChakushin = true
                localStream!!.setEnableAudioTrack(0, false)//Do Something
                mediaConnection!!.answer(localStream)
                setAudioOutput(AudioOutput.Speaker)


                val pref = getSharedPreferences("DataStore", Context.MODE_PRIVATE)
                val editor = pref.edit()

                val text = pref.getString("chakushinMsg", "")

                svRemoteView.visibility = View.VISIBLE
                opendoor.isEnabled = true
                ToiletTextView.text = ""

                if (pref.getBoolean("isChakushin", false)) {
                    if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
                        val vibrationEffect: VibrationEffect = VibrationEffect.createWaveform(
                            longArrayOf(
                                200,
                                200
                            ), 1
                        )
                        vibrator!!.vibrate(vibrationEffect)
                    } else {
                        vibrator!!.vibrate(5000)
                    }
                    runOnUiThread {
                        audioPlay()
                    }

                }

                Handler().postDelayed(Runnable {

                    setHistoryLog(text!!, EntranceImageURL)

                }, 7000)
                runOnUiThread {


                    var Builder =
                        AlertDialog.Builder(this@MainActivity) // FragmentではActivityを取得して生成
                    Builder.setTitle("確認")
                    Builder.setCancelable(false)
                    Builder.setMessage(text)
                    Builder.setPositiveButton("応答", { dialog, which ->
                        // TODO:Yesが押された時の挙動
//                    mediaConnection!!.answer()

                        setAudioOutput(AudioOutput.Speaker)
                        val pref = getSharedPreferences("DataStore", Context.MODE_PRIVATE)

                        outouPush()


                        vibrator!!.cancel()
                        audioStop()
                        val editor = pref.edit()
                        editor.putBoolean("isChakushin", false)

                        localStream!!.setEnableAudioTrack(0, true)//Do Something
                        bConnected = true
                        isOutou = true
                        historyBtn.isEnabled = false
                        CallEntranceBtn.isEnabled = false
                        editor.putBoolean("isOutou", isOutou)
                        editor.apply()
                        waitTV.visibility = View.GONE
                        opendoor.visibility = View.VISIBLE
                        rejectButton.visibility = View.VISIBLE
                        spacer.visibility = View.VISIBLE
                        val str = "応答"
                        var bResult = false


                        Handler().postDelayed(Runnable {

                            try {
//                                if (dataConnection != null) {
                                    bResult = dataConnection!!.send(str)
                                    Log.d("result",bResult.toString())
//                                }
                            }catch(e: Exception){
                                println(e)
                            }


                        }, 2000)

                        if (MaxTimecallHandler != null) {
                            MaxTimecallHandler!!.removeCallbacksAndMessages(null)
                            MaxTimecallHandler = null
                        } else {
                            MaxTimecallHandler = Handler()
                        }

                        MaxTimecallHandler = Handler()
                        MaxTimecallHandler!!.postDelayed(Runnable {

                            disconnect()
                        }, TelMAXTime.toLong() )
                    })
                    Builder.setNegativeButton("拒否", { dialog, which ->


                        var Builder =
                            AlertDialog.Builder(this@MainActivity) // FragmentではActivityを取得して生成
                        Builder.setTitle("確認")
                        Builder.setCancelable(false)
                        Builder.setMessage("本当に拒否しますか？")
                        Builder.setPositiveButton("はい", { dialog, which ->
                            // TODO:Noが押された時の挙動
                            setHistoryLog("着信を拒否しました", EntranceImageURL)
                            val pref = getSharedPreferences("DataStore", Context.MODE_PRIVATE)

                            vibrator!!.cancel()
                            audioStop()
                            val editor = pref.edit()
                            editor.putBoolean("isChakushin", false)
                            editor.apply()
                            val str = "拒否"
                            var bResult = false
                            isOutou = false
                            historyBtn.isEnabled = true
                            CallEntranceBtn.isEnabled = true
                            waitTV.visibility = View.GONE
                            opendoor.visibility = View.GONE
                            rejectButton.visibility = View.GONE
                            spacer.visibility = View.GONE
                            isChakushin = false
                            EntranceImageURL = ""
                            editor.putBoolean("isOutou", isOutou)
                            editor.apply()

                            Handler().postDelayed(Runnable {
                                if (dataConnection != null) {
                                    bResult = dataConnection!!.send(str)
//                                        closeRemoteStream()

                                    Handler().postDelayed(Runnable {
                                        closeRemoteStream()
                                    }, 1000)
                                }

                            }, 2000)

                            Handler().postDelayed(Runnable {

//                                this.finish();
                                this.moveTaskToBack(true);
                                waitTV.visibility = View.VISIBLE
//                                this.finish()
//                                System.exit(0)
//                                this.moveTaskToBack(true);
                            }, 3500)

                            vibrator!!.cancel()
                            audioStop()

                        })
                        Builder.setNegativeButton("いいえ", { dialog, which ->

                            showChakushinAlert()
                        })
                        var kyohidialog = Builder.create()
                        m_ChakushinDialog = kyohidialog
                        kyohidialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
                        val wmlp = kyohidialog.window!!.attributes
                        wmlp.dimAmount = 0.0f
                        wmlp.gravity = Gravity.BOTTOM
//                    wmlp.y = 400
                        kyohidialog.window!!.setFlags(0, WindowManager.LayoutParams.FLAG_DIM_BEHIND)
                        kyohidialog.show()
                        kyohidialog.getButton(AlertDialog.BUTTON_POSITIVE).textSize = 26.0f
                        kyohidialog.getButton(AlertDialog.BUTTON_NEGATIVE).textSize = 26.0f


                    })
                    val chakushindialog = Builder.create()
                    m_ChakushinDialog = chakushindialog
                    chakushindialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
                    val wmlp = chakushindialog.window!!.attributes
                    wmlp.dimAmount = 0.0f
                    wmlp.gravity = Gravity.BOTTOM
//                wmlp.y = 400

                    if (!isFinishing) {
                        chakushindialog.window!!.setFlags(
                            0,
                            WindowManager.LayoutParams.FLAG_DIM_BEHIND
                        )
                        chakushindialog.show()

                        chakushindialog.getButton(AlertDialog.BUTTON_POSITIVE).textSize = 26.0f
                        chakushindialog.getButton(AlertDialog.BUTTON_NEGATIVE).textSize = 26.0f
                    }


                }
            } else {
                Handler().postDelayed(Runnable {
                    dataConnection2!!.send("着信中")
                }, 1000)

            }
        })

        peer!!.on(Peer.PeerEventEnum.OPEN, OnCallback {
            //SkyWayをのPeerIDを取得
            strOwnId = it as String

        })


        peer!!.on(Peer.PeerEventEnum.ERROR, OnCallback {
            val error = it as PeerError
            print(error)

        })

        peer!!.on(Peer.PeerEventEnum.CLOSE, OnCallback {
            isChakushin = false
            EntranceImageURL = ""
        })

        peer!!.on(Peer.PeerEventEnum.DISCONNECTED, OnCallback {


        })

        peer!!.on(Peer.PeerEventEnum.CONNECTION, OnCallback {

            if (isChakushin) {
                dataConnection2 = it as DataConnection
            } else {

                dataConnection = it as DataConnection
                setDataCallbacks()
            }


        })


    }

    fun outouPush(){
        Log.d("応答プッシュ", isMantion.toString())
        val dataStore: SharedPreferences = getSharedPreferences("DataStore", Context.MODE_PRIVATE)
        val today = getToday()
        val user_name = dataStore.getString("user_name", "")
        val message = today + " " + user_name + "が応答しました"
        val propertyID =  dataStore.getString("property_id", "")
        val room_no =  dataStore.getString("room_no", "")

        if(isMantion == true)
        {
            pushURL_Mantion.httpPost(
                listOf(
                    "property" to propertyID,
                    "message" to message,
                    "room_no" to room_no
                )
            ).responseString { request, response, result ->

            }



            Handler().postDelayed(Runnable {
                pushURL_Mantion_ios.httpPost(
                    listOf(
                        "property" to propertyID,
                        "message" to message,
                        "room_no" to room_no
                    )
                ).responseString { request, response, result ->
                    Log.d("a",response.toString())
                }
            }, 500)
        }
        else
        {
            pushURL.httpPost(listOf("property" to propertyID, "message" to message)).responseString { request, response, result ->
                

            }

            Handler().postDelayed(Runnable {
                pushURL_ios.httpPost(
                    listOf(
                        "property" to propertyID,
                        "message" to message
                    )
                ).responseString { request, response, result ->


                }

            }, 500)


        }
//        pushURL.httpPost(listOf("property" to propertyID,"message" to message)).responseString { request, response, result ->
//            pushURL_ios.httpPost(
//                listOf(
//                    "property" to dataStore.getString("property_id ",""),
//                    "message" to message
//                )
//            ).responseString { request, response, result ->
//
//
//            }
//        }
    }

    fun kaijyouPush(){
        val dataStore: SharedPreferences = getSharedPreferences("DataStore", Context.MODE_PRIVATE)
        val today = getToday()
        val user_name = dataStore.getString("user_name", "")
        val message = today + " " + user_name + "が解錠しました"
        val propertyID =  dataStore.getString("property_id", "")
        val room_no =  dataStore.getString("room_no", "")

        if(isMantion == true)
        {
            pushURL_Mantion.httpPost(
                listOf(
                    "property" to propertyID,
                    "message" to message,
                    "room_no" to room_no
                )
            ).responseString { request, response, result ->
                pushURL_Mantion_ios.httpPost(
                    listOf(
                        "property" to dataStore.getString("property_id ", ""),
                        "message" to message,
                        "room_no" to room_no
                    )
                ).responseString { request, response, result ->


                }
            }
        }
        else
        {
            pushURL.httpPost(listOf("property" to propertyID, "message" to message)).responseString { request, response, result ->
                pushURL_ios.httpPost(
                    listOf(
                        "property" to propertyID,
                        "message" to message
                    )
                ).responseString { request, response, result ->


                }
            }
        }

    }

    fun getToday(): String {
        val date = Date()
        val format = SimpleDateFormat("MM月dd日HH時mm分", Locale.getDefault())
        return format.format(date)
    }

    @SuppressLint("ResourceAsColor")
    fun showChakushinAlert()
    {
        val pref = getSharedPreferences("DataStore", Context.MODE_PRIVATE)
        val editor = pref.edit()

        val text = pref.getString("chakushinMsg", "")

        runOnUiThread {


            var Builder = AlertDialog.Builder(this@MainActivity) // FragmentではActivityを取得して生成
            Builder.setTitle("確認")
            Builder.setCancelable(false)
            Builder.setMessage(text)
            Builder.setPositiveButton("応答", { dialog, which ->
                // TODO:Yesが押された時の挙動
//                    mediaConnection!!.answer()

                val pref = getSharedPreferences("DataStore", Context.MODE_PRIVATE)

                setAudioOutput(AudioOutput.Speaker)

                outouPush()



                vibrator!!.cancel()
                audioStop()
                val editor = pref.edit()
                editor.putBoolean("isChakushin", false)
                editor.apply()
                localStream!!.setEnableAudioTrack(0, true)//Do Something
                bConnected = true
                isOutou = true
                historyBtn.isEnabled = false
                CallEntranceBtn.isEnabled = false
                waitTV.visibility = View.GONE
                opendoor.visibility = View.VISIBLE
                rejectButton.visibility = View.VISIBLE
                spacer.visibility = View.VISIBLE
                editor.putBoolean("isOutou", isOutou)
                editor.apply()
                val str = "応答"
                var bResult = false


                Handler().postDelayed(Runnable {
                    bResult = dataConnection!!.send(str)
                }, 2000)


                if (MaxTimecallHandler != null) {
                    MaxTimecallHandler!!.removeCallbacksAndMessages(null)
                    MaxTimecallHandler = null
                } else {
                    MaxTimecallHandler = Handler()
                }

                MaxTimecallHandler = Handler()

                MaxTimecallHandler!!.postDelayed(Runnable {
                    disconnect()
                }, TelMAXTime.toLong())
            })
            Builder.setNegativeButton("拒否", { dialog, which ->


                var Builder = AlertDialog.Builder(this@MainActivity) // FragmentではActivityを取得して生成
                Builder.setTitle("確認")
                Builder.setCancelable(false)
                Builder.setMessage("本当に拒否しますか？")
                Builder.setPositiveButton("はい", { dialog, which ->
                    setHistoryLog("着信を拒否しました", EntranceImageURL)
                    // TODO:Noが押された時の挙動
                    isChakushin = false
                    EntranceImageURL = ""
                    val pref = getSharedPreferences("DataStore", Context.MODE_PRIVATE)
                    isOutou = false
                    historyBtn.isEnabled = true
                    CallEntranceBtn.isEnabled = true
//                    isMantion = false
                    isToilet = false
                    waitTV.visibility = View.GONE
                    opendoor.visibility = View.GONE
                    rejectButton.visibility = View.GONE
                    spacer.visibility = View.GONE
                    editor.putBoolean("isMantion", isMantion)
                    editor.putBoolean("isOutou", isOutou)
                    editor.apply()
                    vibrator!!.cancel()
                    audioStop()
                    val editor = pref.edit()
                    editor.putBoolean("isChakushin", false)
                    editor.apply()
                    val str = "拒否"
                    var bResult = false

                    Handler().postDelayed(Runnable {
                        if (dataConnection != null) {
                            bResult = dataConnection!!.send(str)
//                                        closeRemoteStream()

                            Handler().postDelayed(Runnable {
                                closeRemoteStream()
                            }, 1000)
                        }

                    }, 2000)
                    Handler().postDelayed(Runnable {
//                        this.finish()
//                        System.exit(0)
//                        this.finish();
                        this.moveTaskToBack(true);
                        waitTV.visibility = View.VISIBLE
//                                this.moveTaskToBack(true);
                    }, 3500)

                    vibrator!!.cancel()
                    audioStop()

                })
                Builder.setNegativeButton("いいえ", { dialog, which ->

                    showChakushinAlert()
                })
                var kyohidialog = Builder.create()
                m_ChakushinDialog = kyohidialog
                kyohidialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
                val wmlp = kyohidialog.window!!.attributes
                wmlp.dimAmount = 0.0f
                wmlp.gravity = Gravity.BOTTOM
//                wmlp.y = 400
                kyohidialog.window!!.setFlags(0, WindowManager.LayoutParams.FLAG_DIM_BEHIND)
                kyohidialog.show()
                kyohidialog.getButton(AlertDialog.BUTTON_POSITIVE).textSize = 26.0f
                kyohidialog.getButton(AlertDialog.BUTTON_NEGATIVE).textSize = 26.0f


            })
            val chakushindialog = Builder.create()
            m_ChakushinDialog = chakushindialog
            chakushindialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
            val wmlp = chakushindialog.window!!.attributes
            wmlp.dimAmount = 0.0f
            wmlp.gravity = Gravity.BOTTOM
//            wmlp.y = 400
            chakushindialog.window!!.setFlags(0, WindowManager.LayoutParams.FLAG_DIM_BEHIND)
            chakushindialog.show()
            chakushindialog.getButton(AlertDialog.BUTTON_POSITIVE).textSize = 26.0f
            chakushindialog.getButton(AlertDialog.BUTTON_NEGATIVE).textSize = 26.0f

        }
    }
    fun setDataCallbacks(){
        dataConnection!!.on(DataConnection.DataEventEnum.OPEN, OnCallback {
            bConnected = true


        })
        dataConnection!!.on(DataConnection.DataEventEnum.CLOSE, OnCallback {
            bConnected = false


        })

        dataConnection!!.on(DataConnection.DataEventEnum.DATA, OnCallback {
            var str = ""
            if (it is String) {
                str = it as String
            }

            if (str.contains("物件名")) {
                chakushinName = str
            } else if (str.contains("呼び出しキャンセル")) {
//                isMantion = false
                isToilet = false
                val pref = getSharedPreferences("DataStore", Context.MODE_PRIVATE)
                val editor = pref.edit()
                editor.putBoolean("isMantion", isMantion)
                if (isOutou == true) {
                    Toast.makeText(this@MainActivity, "通話を終了しました", Toast.LENGTH_LONG).show()
                } else {
                    Toast.makeText(this@MainActivity, "呼出をキャンセルしました", Toast.LENGTH_LONG).show()
                }
                isOutou = false
                historyBtn.isEnabled = true
                CallEntranceBtn.isEnabled = true
                editor.putBoolean("isOutou", isOutou)
                editor.apply()
                waitTV.visibility = View.VISIBLE
                opendoor.visibility = View.GONE
                rejectButton.visibility = View.GONE
                spacer.visibility = View.GONE
                isChakushin = false
                EntranceImageURL = ""
                disconnect2()
                vibrator!!.cancel()
                audioStop()

                if (m_ChakushinDialog != null) {
                    m_ChakushinDialog!!.dismiss()
                    m_ChakushinDialog == null
                }
            } else if (str.contains("マンション")) {
                isMantion = true
                val pref = getSharedPreferences("DataStore", Context.MODE_PRIVATE)
                val editor = pref.edit()
                editor.putBoolean("isMantion", isMantion)
                editor.apply()
            } else if (str.contains("最大通話時間")) {
                val arr = str.split(":")
                TelMAXTime = arr[1].toInt()



                Toast.makeText(this, TelMAXTime.toString(), Toast.LENGTH_LONG).show()
            } else if (str.contains("物件ID")) {
                val arr = str.split(":")
                val id = arr[1]
                val pref = getSharedPreferences("DataStore", Context.MODE_PRIVATE)
                val editor = pref.edit()
                editor.putString("ChakushinPropertyID", id)
                editor.apply()
            } else if (str.contains("トイレモード")) {
                isToilet = true
                opendoor.isEnabled = false


                svRemoteView.visibility = View.INVISIBLE

            } else if (str.contains("玄関機名")) {
                val arr = str.split(":")
                val entranceName = arr[1]
                ToiletTextView.text = entranceName
            } else if (str.contains("玄関機画像")) {
                val arr = str.split(",")
                EntranceImageURL = arr[1]

            }

        })

        dataConnection!!.on(DataConnection.DataEventEnum.ERROR, OnCallback {
            val error = it as PeerError
            print(error)
        })
    }
}
