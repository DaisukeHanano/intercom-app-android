package net.nishidentsu.teleark2


import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.os.PersistableBundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import io.skyway.BuildConfig
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_myinfo.*

class MyInfoActivity : AppCompatActivity(){

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_myinfo)

        val pref = getSharedPreferences("DataStore", Context.MODE_PRIVATE)
        val buildingName = pref.getString("buildingName","")
        val room_no = pref.getString("room_no","")
        val id = pref.getString("id","")

        val versionName : String = BuildConfig.VERSION_NAME

        val model = Build.MODEL
        val os = Build.VERSION.RELEASE

        IDTextView.text = id

        ModelTextView.text = model
        OSTextView.text = os
        AppVersionTextView.text = versionName

        buidingNameTextView.text = buildingName

        roomNoTextView.text = room_no
    }

}