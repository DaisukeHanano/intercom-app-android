package net.nishidentsu.teleark2

import android.content.Context
import android.content.Intent
import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.MenuItem
import android.view.View
import android.widget.TextView
import com.github.kittinunf.fuel.httpPost
import com.github.kittinunf.fuel.json.responseJson
import com.github.kittinunf.result.Result
import kotlinx.android.synthetic.main.activity_history.*
import kotlinx.android.synthetic.main.activity_login.*
import org.json.JSONArray
import org.json.JSONObject

class HistoryActivity : AppCompatActivity() {

    val get_property_list_url = "https://webrtc.nishi-dentsu.com/teleark/api/GetPropertyList.php"

    var jsonAry : JSONArray? = JSONArray()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_history)


        val pref = getSharedPreferences("DataStore", Context.MODE_PRIVATE)
        val id = pref.getString("id","")
        get_property_list_url.httpPost(listOf("user_id" to id)).responseJson { request, response, result ->
            when (result) {
                is Result.Success -> {
                    jsonAry = result.value.array()




                    for (i in 0..(jsonAry!!.length()-1))
                    {
                        print(i)
                        var tv : TextView? = TextView(this)

                        if(i == 0)
                        {
                            tv = findViewById(R.id.HistoryTV1)
                        }
                        else if(i == 1){
                            tv = findViewById(R.id.HistoryTV2)
                        }
                        else if(i == 2){
                            tv = findViewById(R.id.HistoryTV3)
                        }
                        else if(i == 3){
                            tv = findViewById(R.id.HistoryTV4)
                        }else if(i == 4){
                            tv = findViewById(R.id.HistoryTV5)
                        }
                        val json = jsonAry!![i] as JSONObject
                        val name = json["name"] as String

                        runOnUiThread {
                            tv!!.text = name
                            tv!!.setBackgroundColor(Color.LTGRAY)
                        }

                    }
                }

                is Result.Failure -> {

                }
            }
        }
    }

    public fun HistoryButtonTapped(view: View)
    {
        if(view == findViewById(R.id.HistoryTV1))
        {
            if(jsonAry!!.length() > 0)
            {
                print("TV1")
                val json = jsonAry!![0] as JSONObject
                val intent = Intent(application,HistoryDetailActivity::class.java)
                intent.putExtra("name",(view as TextView).text)
                intent.putExtra("property_id",json["id"] as String)

                startActivity(intent)
            }

        }
        else if(view == findViewById(R.id.HistoryTV2))
        {
            Log.d("aaa",jsonAry!!.length().toString())
            if(jsonAry!!.length() > 1)
            {
                val json = jsonAry!![1] as JSONObject
                print("TV2")
                val intent = Intent(application,HistoryDetailActivity::class.java)
                intent.putExtra("name",(view as TextView).text)
                intent.putExtra("property_id",json["id"] as String)
                startActivity(intent)
            }

        }
        else if(view == findViewById(R.id.HistoryTV3))
        {
            if(jsonAry!!.length() > 2)
            {
                val json = jsonAry!![2] as JSONObject
                print("TV3")
                val intent = Intent(application,HistoryDetailActivity::class.java)
                intent.putExtra("name",(view as TextView).text)
                intent.putExtra("property_id",json["id"] as String)
                startActivity(intent)
            }

        }
        else if(view == findViewById(R.id.HistoryTV4))
        {
            if(jsonAry!!.length() > 3)
            {
                val json = jsonAry!![3] as JSONObject
                print("TV4")
                val intent = Intent(application,HistoryDetailActivity::class.java)
                intent.putExtra("name",(view as TextView).text)
                intent.putExtra("property_id",json["id"] as String)
                startActivity(intent)
            }

        }
        else if(view == findViewById(R.id.HistoryTV5))
        {
            if(jsonAry!!.length() > 4)
            {
                val json = jsonAry!![4] as JSONObject
                print("TV5")
                val intent = Intent(application,HistoryDetailActivity::class.java)
                intent.putExtra("name",(view as TextView).text)
                intent.putExtra("property_id",json["id"] as String)
                startActivity(intent)
            }

        }
    }

    override  fun onResume()
    {
        super.onResume()
        supportActionBar!!.title = "履歴"
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if(item.itemId == android.R.id.home)
        {
            finish()
            return  true
        }
        return super.onOptionsItemSelected(item)

    }

}